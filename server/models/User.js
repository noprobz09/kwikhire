const mongoose = require('mongoose');
const { Schema } = mongoose;

// User Schema
const UserSchema = new Schema({
   
    company: String,
    fullname: String,
    email: String,
    phone: String,
    password: String,

});

mongoose.model("users", UserSchema);